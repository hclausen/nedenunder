
	jQuery(document).ready(function($){
									
				function swapButton(){
					var txtswap = $(".form-footer button[type='submit']");
					if (txtswap.text() == txtswap.data("btntext-sending")) {
						txtswap.text(txtswap.data("btntext-original"));
					} else {
						txtswap.data("btntext-original", txtswap.text());
						txtswap.text(txtswap.data("btntext-sending"));
					}
				}
				

				function recaptchaResetCallback () {
					if($('#g-recaptcha').length) { 
						grecaptcha.reset(); 
					}
				}
			   
				$( "#smart-form" ).validate({
						errorClass: "state-error",
						validClass: "state-success",
						errorElement: "em",
						rules: {
								sendername: {
										required: true,
										minlength: 2
								},		
								emailaddress: {
										required: true,
										email: true
								},
								sendersubject: {
										required: true,
										minlength: 4
								},								
								sendermessage: {
										required: true,
										minlength: 10
								},
								"g-recaptcha-response":{
									required:true,
									remote:'./smart-form/contact-recaptcha/php/process_reCaptcha.php'
								}
						},
						messages:{
								sendername: {
                                    required: 'Du skal indtaste et navn.',
                                    minlength: 'Navnet skal v�re mindst 2 bogstaver.'
								},				
								emailaddress: {
                                    required: 'Du skal indtaste en e-mail.',
                                    email: 'Din e-mail adresse ser ikke ud til at v�re gyldig.'
								},
								sendersubject: {
                                    required: 'Du skal indtaste et emne.',
                                    minlength: 'Emne feltet skal v�re mindst 4 bogstaver.'
								},														
								sendermessage: {
										required: 'Hovsa! Det ser ud til du glemte at skrive en besked.',
										minlength: 'Din besked b�r v�re mindst 10 bogstaver.'
								},															
								"g-recaptcha-response":{
										required: 'Check reCaptcha for verification',
										remote:'Invalid reCaptcha verification'
								}
						},
						highlight: function(element, errorClass, validClass) {
								$(element).closest('.field').addClass(errorClass).removeClass(validClass);
						},
						unhighlight: function(element, errorClass, validClass) {
								$(element).closest('.field').removeClass(errorClass).addClass(validClass);
						},
						errorPlacement: function(error, element) {
						   if (element.is(":radio") || element.is(":checkbox")) {
									element.closest('.option-group').after(error);
						   } else {
									error.insertAfter(element.parent());
						   }
						},				
						submitHandler:function(form) {
							$(form).ajaxSubmit({
								    target:'.result',	
									beforeSubmit:function(){ 
											swapButton();
											$('.form-footer').addClass('progress');
									},
									error:function(){
											swapButton();
											$('.form-footer').removeClass('progress');
									},
									 success:function(){
										 	swapButton();
											$('.form-footer').removeClass('progress');
											$('.alert-success').show().delay(7000).fadeOut();
											$('.field').removeClass("state-error, state-success");
											if( $('.alert-error').length == 0){
												$('#smart-form').resetForm();
												recaptchaResetCallback();
											}
									 }
							  });
						}
				});	
		 
		
	});				
    