﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;

namespace Nedenunder.Models
{
    public class EmailSender : IEmailSender
    {
        private readonly string _host;
        private readonly int _port;
        private readonly bool _enableSSL;
        private readonly string _userName;
        private readonly string _password;
        private readonly string _displayName;

        public EmailSender(string host, int port, bool enableSsl, string userName, string password, string displayName)
        {
            this._host = host;
            this._port = port;
            this._enableSSL = enableSsl;
            this._userName = userName;
            this._password = password;
            this._displayName = displayName;
        }
        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            try
            {
                var emailAddress = new MailAddress(this._userName, this._displayName);
                var toEmail = new MailAddress("bartiformand@gmail.com");
                var message = new MailMessage(emailAddress, toEmail)
                {
                    Subject = subject,
                    Body = htmlMessage,
                    IsBodyHtml = true
                };

                using (var client = new SmtpClient(this._host, this._port))
                {
                    client.Credentials = new NetworkCredential(this._userName, this._password);
                    client.EnableSsl = this._enableSSL;

                    await client.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {
                // TODO: cba atm
                throw new InvalidOperationException(ex.Message);
            }
        }
    }
}
