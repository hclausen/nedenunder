$(document).ready(function () {
// Simple map
    map = new GMaps({
        scrollwheel: false,
        el: '#gmaps-simple',
        lat: 55.3676614,
        lng: 10.42804509,
        zoom: 8,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        overviewMapControl: false
    });
});

// Marker Map


$(document).ready(function () {
    map = new GMaps({
        scrollwheel: false,
        el: '#markermap',
        lat: 55.3676614,
        lng: 10.42804509

    });
    map.addMarker({
        lat: 55.3676614,
        lng: 10.42804509,
        title: 'Marker with InfoWindow',
        infoWindow: {
            content: '<p>Your Content</p>'
        }
    });
});
