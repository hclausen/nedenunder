#pragma checksum "C:\work\Nedenunder\Nedenunder\Views\Shared\_Layout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8a66e4c787e87f0e648c7d9a9a6c9b05d21e1f26"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__Layout), @"mvc.1.0.view", @"/Views/Shared/_Layout.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_Layout.cshtml", typeof(AspNetCore.Views_Shared__Layout))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\work\Nedenunder\Nedenunder\Views\_ViewImports.cshtml"
using Nedenunder;

#line default
#line hidden
#line 2 "C:\work\Nedenunder\Nedenunder\Views\_ViewImports.cshtml"
using Nedenunder.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a66e4c787e87f0e648c7d9a9a6c9b05d21e1f26", @"/Views/Shared/_Layout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"89669a82bbb0791e254ec5c213f1b06266108f2e", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Layout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-spy", new global::Microsoft.AspNetCore.Html.HtmlString("scroll"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-target", new global::Microsoft.AspNetCore.Html.HtmlString("#navbarsspy"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-offset", new global::Microsoft.AspNetCore.Html.HtmlString("57"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 25, true);
            WriteLiteral("<!DOCTYPE html>\r\n<html>\r\n");
            EndContext();
            BeginContext(25, 418, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8a66e4c787e87f0e648c7d9a9a6c9b05d21e1f264350", async() => {
                BeginContext(31, 405, true);
                WriteLiteral(@"
    <!-- Required meta tags -->
    <meta charset=""utf-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1, shrink-to-fit=no"">
    <title>Nedenunder - Studenterbaren på SDU</title>
    <link href=""css/plugins/plugins.css"" rel=""stylesheet"">
    <link href=""smart-form/contact-recaptcha/css/smart-forms.css"" rel=""stylesheet"">
    <link href=""css/style.css"" rel=""stylesheet"">
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(443, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(445, 5233, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8a66e4c787e87f0e648c7d9a9a6c9b05d21e1f265953", async() => {
                BeginContext(512, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(521, 12, false);
#line 14 "C:\work\Nedenunder\Nedenunder\Views\Shared\_Layout.cshtml"
Write(RenderBody());

#line default
#line hidden
                EndContext();
                BeginContext(533, 5138, true);
                WriteLiteral(@"
    
    <script type=""text/javascript"" src=""js/plugins/plugins.js""></script>
    <script type=""text/javascript"" src=""js/onepage.custom.js""></script>
    <script type=""text/javascript"" src=""smart-form/contact-recaptcha/js/jquery.form.min.js""></script>
    <script type=""text/javascript"" src=""smart-form/contact-recaptcha/js/jquery.validate.min.js""></script>
    <script type=""text/javascript"" src=""smart-form/contact-recaptcha/js/additional-methods.min.js""></script>
    <script type=""text/javascript"" src=""smart-form/contact-recaptcha/js/smart-form.js""></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type=""text/javascript"">
        jQuery(document).ready(function ($) {

            function swapButton() {
                var txtswap = $("".form-footer button[type='submit']"");
                if (txtswap.text() == txtswap.data(""btntext-sending"")) {
                    txtswap.text(txtswap.data(""btntext-original""));
                } else {
                  ");
                WriteLiteral(@"  txtswap.data(""btntext-original"", txtswap.text());
                    txtswap.text(txtswap.data(""btntext-sending""));
                }
            }


            function recaptchaResetCallback() {
                if ($('#g-recaptcha').length) {
                    grecaptcha.reset();
                }
            }

            $(""#smart-form"").validate({
                errorClass: ""state-error"",
                validClass: ""state-success"",
                errorElement: ""em"",
                rules: {
                    sendername: {
                        required: true,
                        minlength: 2
                    },
                    emailaddress: {
                        required: true,
                        email: true
                    },
                    sendersubject: {
                        required: true,
                        minlength: 4
                    },
                    sendermessage: {
                        required: true,");
                WriteLiteral(@"
                        minlength: 10
                    },
                    ""g-recaptcha-response"": {
                        required: true,
                        remote: './smart-form/contact-recaptcha/php/process_reCaptcha.php'
                    }
                },
                messages: {
                    sendername: {
                        required: 'Du skal indtaste et navn.',
                        minlength: 'Navnet skal være mindst 2 bogstaver.'
                    },
                    emailaddress: {
                        required: 'Du skal indtaste en e-mail.',
                        email: 'Din e-mail adresse ser ikke ud til at være gyldig.'
                    },
                    sendersubject: {
                        required: 'Du skal indtaste et emne.',
                        minlength: 'Emne feltet skal være mindst 4 bogstaver.'
                    },
                    sendermessage: {
                        required: 'Hovsa! Det ser ud t");
                WriteLiteral(@"il du glemte at skrive en besked.',
                        minlength: 'Din besked bør være mindst 10 bogstaver.'
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },
                errorPlacement: function (error, element) {
                    if (element.is("":radio"") || element.is("":checkbox"")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                },
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        target: '.result',
     ");
                WriteLiteral(@"                   beforeSubmit: function () {
                            swapButton();
                            $('.form-footer').addClass('progress');
                        },
                        error: function () {
                            swapButton();
                            $('.form-footer').removeClass('progress');
                        },
                        success: function () {
                            swapButton();
                            $('.form-footer').removeClass('progress');
                            $('.alert-success').show().delay(7000).fadeOut();
                            $('.field').removeClass(""state-error, state-success"");
                            if ($('.alert-error').length == 0) {
                                $('#smart-form').resetForm();
                                recaptchaResetCallback();
                            }
                        }
                    });
                }
            });
        });		");
                WriteLiteral("\n    </script>\r\n\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5678, 11, true);
            WriteLiteral("\r\n</html>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
