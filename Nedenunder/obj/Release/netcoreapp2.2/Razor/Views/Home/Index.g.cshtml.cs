#pragma checksum "C:\work\Nedenunder\Nedenunder\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "20e61ab0cbf8e8c3ff76f51df6a8302d8e38b2d5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\work\Nedenunder\Nedenunder\Views\_ViewImports.cshtml"
using Nedenunder;

#line default
#line hidden
#line 2 "C:\work\Nedenunder\Nedenunder\Views\_ViewImports.cshtml"
using Nedenunder.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"20e61ab0cbf8e8c3ff76f51df6a8302d8e38b2d5", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"89669a82bbb0791e254ec5c213f1b06266108f2e", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("smart-form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2911, true);
            WriteLiteral(@"
<div id=""preloader"">
    <div id=""preloader-inner""></div>
</div><!--/preloader-->

<section id=""home"" class=""bg-parallax fullscreen parallax-overlay"" data-jarallax-video=""https://youtu.be/dUFNpTDYWYc"" data-jarallax-orignal-styles=""null"">
    <div class=""content-table"">
        <div class=""content-middle pt80 "">
            <div class=""container"">
                <div class=""row"">
                    <div class=""col-lg-12 col-md-12 ml-auto mr-auto text-center"">
                        <h2 class=""hero-big-text text-white"">Velkommen til Nedenunder!</h2>
                        <p class=""lead text-white-gray"">
                            Studenterbaren på Syddansk Universitet
                        </p>
                        <div class=""circle-button"">
                            <a href=""#about"" data-scroll class=""btn btn-white-outline btn-lg btn-rounded"">Klik her!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--he");
            WriteLiteral(@"ro inner-->
    </div><!--parallax hero-->
    <div class=""mouse-down""><a data-scroll href=""#about""><i class=""fa fa-angle-down fa-3x""></i></a></div>
</section>
<!--intro section end-->
<nav class=""navbar navbar-expand-lg navbar-light bg-white sticky-nav"">
    <div class=""container"">
        <button class=""navbar-toggler navbar-toggler-right"" type=""button"" data-toggle=""collapse"" data-target=""#navbarsspy"" aria-controls=""navbarsspy"" aria-expanded=""false"" aria-label=""Toggle navigation"">
            <span class=""navbar-toggler-icon""></span>
        </button>
        <a class=""navbar-brand"" data-scroll href=""#home""><img src=""images/logo_big.png"" alt="""" style=""max-height: 50px; max-width: 250px;""></a>
        <div class=""collapse navbar-collapse"" id=""navbarsspy"">
            <ul class=""navbar-nav ml-auto"">
                <li class=""nav-item""><a data-scroll class=""nav-link active"" href=""#home"">home</a></li>
                <li class=""nav-item""><a data-scroll class=""nav-link"" href=""#about"">Om os</a></li");
            WriteLiteral(@">
                <li class=""nav-item""><a data-scroll class=""nav-link"" href=""#news"">Nyheder</a></li>
                <li class=""nav-item""><a data-scroll class=""nav-link"" href=""#socialmedia"">Sociale Medier</a></li>
                <li class=""nav-item""><a data-scroll class=""nav-link"" href=""#sponsors"">Sponsorer</a></li>
                <li class=""nav-item""><a data-scroll class=""nav-link"" href=""#contact"">Kontakt</a></li>
            </ul>
        </div>
        <ul class=""float-right navbar-nav hidden-md-down"">
            <li class=""nav-item""><a href=""http://eepurl.com/vlwq5"" target=""_blank"" class=""nav-link btn btn-rounded btn-primary"">Bliv medlem</a></li>
        </ul>
    </div>
</nav>
<section id=""about"" class=""pt90 pb50"">
    <div class=""container"">
        <div class=""row align-items-center"">
            <div class=""col-md-12 mb40"">
");
            EndContext();
            BeginContext(3017, 3369, true);
            WriteLiteral(@"                <div class=""section-title title-left mb40 text-left"">
                    <span class=""section-subTitle"">Velkommen til Nedenunder - Studenterbaren på SDU</span>
                    <h4 class='mb0 h3 font300 mb20'>Nedenunder - stedet hvor studie bliver til velvære, hvor pensum glemmes og tømmermænd skabes!</h4>
                    <p class=""lead"">
                        Her møder du alt fra de store talnørder til sprogfreaken. <br/>
                        Vi serverer dejlig og billig øl i hyggelige omgivelser! (PS. øllene er naturligvis fra Albani!)
                    </p>
                </div>
            </div>
        </div>
        
    </div>
</section><!--section end about-->

<section id=""news"" class=""bg-faded pt90 pb60"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-md-6 ml-auto mr-auto text-center mb30"">
                <div class=""section-title"">
                    <h3 class=""mb0"">Nyheder</h3>
                </div>
       ");
            WriteLiteral(@"     </div>
        </div>
        <div class='row no-margin'>
            <div class='col-md-4 mb40'>
                <img src='images/logo_square.png' alt='' class='img-fluid center-img shadow-card mb30' style=""height: 230px; width: 600px;"">
                <h5 class='text-uppercase'>Det sker:</h5>
                <p>
                    Du kan følge din fredagsbars aktiviteter på vores <a href=""https://www.facebook.com/detskerNedenunder/"" target=""_blank""> Facebookside</a>
                </p>
            </div>
            <div class='col-md-4 mb40'>
                <img src='images/logo_square.png' alt='' class='img-fluid center-img shadow-card mb30' style=""height: 230px; width: 600px;"">
                <h5 class='text-uppercase'>Nyt fra generalforsamlingen</h5>
                <p>
                    Se nedenstående for at blive opdateret på Nedenunders økonomi samt vedtægter <br />
                    <a href=""/files/Vedtægter.pdf"" target=""_blank"">Vedtægter anno 2019</a> <br />
         ");
            WriteLiteral(@"           <a href=""/files/Årsrapport-2018.pdf"" target=""_blank"">Regnskab 2018</a> <br />
                    <a href=""/files/Budget-2019.pdf"" target=""_blank"">Budget 2019</a> <br />
                </p>
            </div>
            <div class='col-md-4 mb40'>
                <img src='images/logo_square.png' alt='' class='img-fluid center-img shadow-card mb30' style=""height: 230px; width: 600px;"">
                <h5 class='text-uppercase'>Bliv medlem</h5>
                <p>
                    Du kan nu melde dig ind i baren, og dermed modtage nyhedsbreve, info om arrangementer og deltage i generalforsamlinger.
                </p>
                <p><a href=""http://eepurl.com/vlwq5"" target=""_blank"">Klik her for at melde dig ind</a></p>
            </div>

        </div>
    </div>
</section><!--news-->

<section id=""socialmedia"" class=""pt90 pb60"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-md-6 ml-auto mr-auto text-center mb30"">
                <");
            WriteLiteral(@"div class=""section-title"">
                    <h3 class=""mb0"">Her kan du finde os på de sociale medier</h3>
                </div>
            </div>
        </div>
        <div class=""row no-margin"">
            <!-- Snapchat? other? -->
            <div class=""col-lg-3 col-md-6 mb30"">
");
            EndContext();
            BeginContext(6892, 1295, true);
            WriteLiteral(@"            </div><!--/col-->

            <div class=""col-lg-3 col-md-6 mb30"">
                <div class=""team-card"">
                    <a href=""https://www.facebook.com/detskerNedenunder/"" target=""_blank"">
                        <img src=""images/facebook.jpg"" alt="""" class=""img-fluid"">
                        <div class=""team-overlay align-items-center"">
                            <div class=""team-detail"">
                                <h4>Facebook</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div><!--/col-->
            <div class=""col-lg-3 col-md-6 mb30"">
                <div class=""team-card"">
                    <a href=""https://www.instagram.com/nedenundersdu/"" target=""_blank"">
                        <img src=""images/instagram.png"" alt="""" class=""img-fluid"">
                        <div class=""team-overlay align-items-center"">
                            <div class=""team-detail"">
           ");
            WriteLiteral(@"                     <h4>Instagram</h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div><!--/col-->
            <!-- Youtube? -->
            <div class=""col-lg-3 col-md-6 mb30"">
");
            EndContext();
            BeginContext(8693, 3075, true);
            WriteLiteral(@"            </div><!--/col-->
        </div>
    </div>
</section><!--team-->
<section id=""sponsors"" class=""bg-faded pt90 pb60"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-sm-8 mr-auto ml-auto text-center"">
                <div class=""center-title mb60"">
                    <h2>Sponsorer</h2>
                    <p>Uden disse kunne vi ikke være til!</p>
                </div>
            </div>
        </div>
        <div class=""row sponsors-row"">
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""#"">
                    <img src="""" alt="""" class=""img-fluid"">
                </a>
            </div>
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""#"">
                    <img src="""" alt="""" class=""img-fluid"">
                </a>
            </div>
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""https://www.sdu.dk"">
                    ");
            WriteLiteral(@"<img src=""images/sdu.png"" alt="""" class=""img-fluid"">
                </a>
            </div>
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""https://www.albani.dk"">
                    <img src=""images/albani.svg"" alt="""" class=""img-fluid"">
                </a>
            </div>
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""#"">
                    <img src="""" alt="""" class=""img-fluid"">
                </a>
            </div>
            <div class=""col-lg-2 col-md-3 col-sm-6 col-xs-6"">
                <a href=""#"">
                    <img src="""" alt="""" class=""img-fluid"">
                </a>
            </div>
        </div>
    </div>
</section><!--sponsors-->

<section id=""contact"" class=""pt90 pb50"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-md-5 pb40"">
                <div class=""section-title title-left mb30"">
                    <h3 class=""mb0"">
               ");
            WriteLiteral(@"         Kontakt
                    </h3>
                </div>
                <ul class=""list-unstyled contact-info"">
                    <li>
                        <i class=""fa fa-home""></i>
                        Campusvej 55<br>
                        5230 Odense M<br>
                        Indgang C
                    </li>
                    <li>
                        <i class=""fa fa-phone""></i>
                        <a href=""#"">+45 28 91 74 75</a>
                    </li>
                    <li>
                        <i class=""fa fa-envelope""></i>
                        <a href=""mailto:bartiformand@gmail.com"">bartiformand@gmail.com</a>
                    </li>
                </ul>
            </div>
            <div class=""col-md-6 offset-md-1 pb40"">
                <h3 class=""font300 mb30"">Har du spørgsmål? Så kontakt os!</h3>
                <div class=""smart-wrap"">
                    <div class=""smart-forms smart-container wrap-2"">
                     ");
            WriteLiteral("   ");
            EndContext();
            BeginContext(11768, 2735, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "20e61ab0cbf8e8c3ff76f51df6a8302d8e38b2d515479", async() => {
                BeginContext(11840, 2656, true);
                WriteLiteral(@"
                            <div class=""form-body"">
                                <div class=""section"">
                                    <label class=""field prepend-icon"">
                                        <input type=""text"" name=""name"" id=""sendername"" class=""gui-input"" placeholder=""Indtast navn"">
                                        <span class=""field-icon""><i class=""fa fa-user""></i></span>
                                    </label>
                                </div><!-- end section -->
                                <div class=""section"">
                                    <label class=""field prepend-icon"">
                                        <input type=""email"" name=""email"" id=""emailaddress"" class=""gui-input"" placeholder=""Indtast e-mail"">
                                        <span class=""field-icon""><i class=""fa fa-envelope""></i></span>
                                    </label>
                                </div><!-- end section -->
                         ");
                WriteLiteral(@"       <div class=""section"">
                                    <label class=""field prepend-icon"">
                                        <input type=""text"" name=""subject"" id=""sendersubject"" class=""gui-input"" placeholder=""Indtast emne"">
                                        <span class=""field-icon""><i class=""fa fa-lightbulb-o""></i></span>
                                    </label>
                                </div><!-- end section -->
                                <div class=""section"">
                                    <label class=""field prepend-icon"">
                                        <textarea class=""gui-textarea"" id=""sendermessage"" name=""message"" placeholder=""Indtast besked""></textarea>
                                        <span class=""field-icon""><i class=""fa fa-comments""></i></span>
                                        <span class=""input-hint""> <strong>Hint:</strong> Hold venligst din besked til maksimum 300 bogstaver</span>
                                    </labe");
                WriteLiteral(@"l>
                                </div><!-- end section -->
                                <div class=""result""></div><!-- end .result  section -->
                            </div><!-- end .form-body section -->
                            <div class=""form-footer"">
                                <button type=""submit"" data-btntext-sending=""Sending..."" class=""button btn btn-primary"">Indsend</button>
                                <button type=""reset"" class=""button btn-secondary""> Annuller </button>
                            </div><!-- end .form-footer section -->
                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "action", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 246 "C:\work\Nedenunder\Nedenunder\Views\Home\Index.cshtml"
AddHtmlAttributeValue("", 11796, Url.Action("PostMessage"), 11796, 26, false);

#line default
#line hidden
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(14503, 654, true);
            WriteLiteral(@"
                    </div><!-- end .smart-forms section -->
                </div><!-- end .smart-wrap section -->
            </div>
        </div>
    </div>
</section>

<footer class='footer' style=""background-color: #212121"">
    <div class='container'>
        <div class='row'>
            <div class='col-md-12 text-md-center text-center'>
                <span>&copy; Copyright 2019. <a href=""http://www.eventii.dk"">eventii - Events your way</a></span>
            </div>
        </div>
    </div>
</footer>
<!--back to top-->
<a href=""#"" class=""back-to-top hidden-xs-down"" id=""back-to-top""><i class=""fa fa-angle-up""></i></a>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
