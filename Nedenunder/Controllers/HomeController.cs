﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nedenunder.Models;

namespace Nedenunder.Controllers
{
    public class HomeController : Controller
    {
        private readonly IViewRenderService _viewRenderService;
        private readonly IEmailSender _emailSender;

        public HomeController(IViewRenderService viewRenderService, IEmailSender emailSender)
        {
            this._viewRenderService = viewRenderService;
            this._emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> PostMessage(MessageViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    var emailMessage = await this._viewRenderService.RenderToStringAsync("Shared/EmailTemplate", model);
                    await this._emailSender.SendEmailAsync(string.Empty, "Ny besked fra hjemmesiden!", emailMessage);
                    return this.Ok("<div class=\"alert notification alert-success\">Din besked er sendt!</div>");
                }
                catch (Exception ex)
                {
                    return this.Ok("<div class=\"alert notification alert-error\">Der skete en fejl! Din besked er desværre ikke blevet sendt :(</div>");
                }
            }

            return this.Ok("<div class=\"alert notification alert-error\">Der skete en fejl! Din besked er desværre ikke blevet sendt :(</div>");
        }
    }
}
